<?php

namespace Skimia\News\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\News\Data\Models\Post;

class PostsCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Post();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.news::form.news');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('name')
            ->transAll()
            ->setDisplayOrder(1000);

        $options->Fields()->makeRelationField('category')
            ->transAll()
            ->ManyToOneRelation()
            ->displayColumns(['name'])
            ->setGenerating(function(Options\Fields\RelationField $field){
                    $field->displayColumns(['isolated','name']);
                $field->setBoolValuesForColumnNames('isolated','(Isolée)','');
            })
            ->setDisplayOrder(1000);

        $options->Fields()->makeImageField('picture')->transAll();


        $options->Fields()->makeWYSIWYGField('content_short')
            ->transAll()
            ->setDisplayOrder(1000);

        $options->Fields()->makeWYSIWYGField('content')
            ->transAll()
            ->setDisplayOrder(1000);


    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-newspaper')->setTitle('Liste de vos Actualités');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-newspaper')->setTitle('Créer une nouvelle Actualité');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-newspaper')->setTitle('Editer votre Actualité');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);


        $this->listConfiguration->addIdColumn();
        $image = $this->listConfiguration->getNewColumnDefinition('picture')->type(ListCrudColumnConfiguration::_TYPE_PICTURE)->automaticTranslatedDisplayName();
        $name = $this->listConfiguration->getNewColumnDefinition('name')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $slug = $this->listConfiguration->getNewColumnDefinition('slug')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $slug = $this->listConfiguration->setEmptyMessage('Pour créer de nouveaux articles veuillez cliquer sur le bouton Ajouter ci-dessus','Aucun article disponnible');



        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Actualité "%name%" edité');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouvelle actualité "%name%" crée');





    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'n_posts';
    }

}