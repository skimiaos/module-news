<?php

namespace Skimia\News\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\News\Data\Models\Category;
use Skimia\News\Data\Models\Post;

class CategoriesCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Category();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.news::form.categories');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('name')
            ->transAll()
            ->setDisplayOrder(1000);

            $options->Fields()->makeCheckboxField('isolated')
                ->transAll()->setGenerating(function(Options\Fields\BaseField $field){
                    if(!$this->getAcl()->can('advanced_use'))
                        $field->hide();
                })
                ->setDisplayOrder(1000);

    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-newspaper')->setTitle('Catégories de vos Actualités');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-newspaper')->setTitle('Créer une nouvelle Catégorie');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-newspaper')->setTitle('Renommer votre catégorie');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);



        $this->listConfiguration->addIdColumn();
        $name = $this->listConfiguration->getNewColumnDefinition('name')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $name = $this->listConfiguration->getNewColumnDefinition('slug')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();


        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Catégorie "%name%" sauvegardée');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouvelle catégorie "%name%" crée');





    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'n_categories';
    }

}