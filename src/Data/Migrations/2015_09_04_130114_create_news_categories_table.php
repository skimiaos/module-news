<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('news_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('slug', 255)->unique();
			$table->boolean('isolated')->default(false);
			$table->string('name', 255);
		});
		$category = new \Skimia\News\Data\Models\Category();
		$category->name = 'Categorie par défaut';
		$category->save();
	}

	public function down()
	{
		Schema::drop('news_categories');
	}
}