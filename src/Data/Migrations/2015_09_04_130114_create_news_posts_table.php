<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsPostsTable extends Migration {

	public function up()
	{
		Schema::create('news_posts', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('slug', 255)->unique();
			$table->string('name', 255);
			$table->text('content_short');
			$table->text('content');
			$table->timestamp('publish_date');
			$table->text('picture');
			$table->integer('category_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('news_posts');
	}
}