<?php

namespace Skimia\News\Data\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Eloquent;
class Category extends Eloquent implements SluggableInterface{

	use SluggableTrait;

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);

	protected $table = 'news_categories';
	public $timestamps = true;
	protected $fillable = array('slug', 'name');

	public function posts()
	{
		return $this->hasMany('Skimia\News\Data\Models\Post', 'category_id');
	}


}