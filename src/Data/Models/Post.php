<?php

namespace Skimia\News\Data\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use League\Flysystem\Exception;

class Post extends Eloquent implements SluggableInterface{


	use SluggableTrait;

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);

	protected $table = 'news_posts';
	public $timestamps = true;
	protected $fillable = array('slug', 'name', 'content_short', 'content', 'publish_date', 'picture');

	public function category()
	{
		return $this->belongsTo('Skimia\News\Data\Models\Category');
	}


	public static function getForThumbs($thumbs_count = 3){
		return Post::with('category')
			->where('publish_date', '<=', Carbon::now())
			->whereHas('category', function($q)
			{
				$q->where('isolated', false);

			})
			->orderBy('created_at','desc')
			->take($thumbs_count)
			->get(['id','name','picture','slug']);
	}

	public static function pagin($pgna,$filter = false){
		$qb =  Post::with('category')->where('publish_date', '<=', Carbon::now())->orderBy('created_at','desc');

		if($filter){
			$cat = Category::where('slug',$filter)->first();
			if($cat == null)
				return $qb->where('category_id',-1)->paginate($pgna);
			return $qb->where('category_id',$cat->id)->paginate($pgna);
		}else{
			$qb->whereHas('category', function($q)
			{
				$q->where('isolated', false);

			});
		}

		return $qb->paginate($pgna);
	}
	public static function findBySlug( $slug ){
		$post = Post::with('category')
			->where('publish_date', '<=', Carbon::now())
			->where('slug',$slug)

			->get()->first();
		//dd($post);
		if($post)
			return $post;

		\App::abort(404);
	}
}