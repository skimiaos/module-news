<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\News\Components;

use Illuminate\Support\Collection;
use Skimia\News\Data\Models\Post;
use Skimia\Pages\Components\Component;
use Skimia\Pictures\Data\Models\Slider\Slider as SliderEntity;
class PostsPage extends Component{

    protected static $systemName = 'page_news_posts';

    protected static $page = [
        'url'=>'post/{slug}',
        'patterns'=>[
            'slug'=> '[a-z0-9-]+'
        ]
    ];

    protected $name = 'Page un Article';
    protected $description = 'Page affichant un article';
    protected $icon = 'os-icon-newspaper';

    protected $show_template = 'skimia.news::components.news.get_page';


    protected function makeFields(){

        $this->fields = [
           /* 'slider_name'=>[
                'type'=>'select',
                'label'=>'choix du slider',
                'choicesFct'=> function(){
                    return SliderEntity::lists('identifier','id');
                }
            ],
            'loop'=>[
                'type'=>'checkbox',
                'label'=>'Boucle infinie',
                'default'=>true
            ],
            'nav'=>[
                'type'=>'checkbox',
                'label'=>'Afficher les boutons suivant/precedent',
                'default'=>false
            ],
            'dots'=>[
                'type'=>'checkbox',
                'label'=>'Afficher les bulles de navigation',
                'default'=>true
            ],
            'lazyload'=>[
                'type'=>'checkbox',
                'label'=>'Chargement dynamique des images',
                'default'=>true
            ],

            'autoplay'=>[
                'type'=>'checkbox',
                'label'=>'Lecture automatique',
                'default'=>false
            ],
            'autoplayHoverPause'=>[
                'type'=>'checkbox',
                'label'=>'Pause au survol',
                'default'=>false
            ],
            'thumbs_count'=>[
                'type'=>'text',
                'label'=>'Nombre dActualités a afficher',
                'default'=>'3'
            ],*/
        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        $merged = $this->position->getConfiguration();

        $merged['actu'] = Post::findBySlug(\Route::input('slug'));

        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected $fields  = [

    ];

    public function getStaticJS()
    {
        return '';
        return file_get_contents(module_assets('skimia.newsletter','/components/newsletter/news.js'));
    }

    public function getDynJS()
    {
        //dd($this->onJavascript());
        return '';
    }

    public function getStaticCSS()
    {
        return '';
        return file_get_contents(module_assets('skimia.newsletter','/components/newsletter/news.css'));
    }

    public function getDynCSS()
    {
        return '';
    }
}