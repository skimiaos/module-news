<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\News\Components;

use Illuminate\Support\Collection;
use Skimia\News\Data\Models\Category;
use Skimia\News\Data\Models\Post;
use Skimia\Pages\Components\Component;
use Skimia\Pictures\Data\Models\Slider\Slider as SliderEntity;
class ListingPage extends Component{

    protected static $systemName = 'page_news_listing';

    protected static $page = [
        'url'=>'posts'
    ];

    protected $name = 'Page liste Actus';
    protected $description = 'Page affichant la liste des actus';
    protected $icon = 'os-icon-newspaper';

    protected $show_template = 'skimia.news::components.news.list_page';


    protected function makeFields(){

        $this->fields = [

            'actus_link'=>[
                'type'=>'component-page-link',
                'label'=>'Page des articles',
                'sysName'=> 'page_news_posts',
                'default'=>false
            ],
            'restrict_filter'=>[
                'type'=>'text',
                'label'=>'Slug de la catégorie restrictive',
                'default'=>''
            ],
            'actus_count'=>[
                'type'=>'text',
                'label'=>'Nombre dActualités a afficher',
                'default'=>'4'
            ]
        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        $merged = $this->position->getConfiguration();

        $merged['categories'] = Category::where('isolated',false)->get();
        if(($filter = (isset($merged['restrict_filter']) &&!empty($merged['restrict_filter']))?$merged['restrict_filter'] : \Input::get('filter', false))){

            $merged['actus'] = Post::pagin(isset($merged['actus_count']) ?$merged['actus_count']:4,$filter);
        }else
            $merged['actus'] = Post::pagin(isset($merged['actus_count']) ?$merged['actus_count']:4);

        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected $fields  = [

    ];

    public function getStaticJS()
    {
        return '';
        return file_get_contents(module_assets('skimia.newsletter','/components/newsletter/news.js'));
    }

    public function getDynJS()
    {
        //dd($this->onJavascript());
        return '';
    }

    public function getStaticCSS()
    {
        return '';
        return file_get_contents(module_assets('skimia.newsletter','/components/newsletter/news.css'));
    }

    public function getDynCSS()
    {
        return '';
    }
}