<?php

return [
    'name'        => 'Undefined Module',
    'author'      => 'Undefined Author',
    'description' => 'Undefined Description',
    'namespace'   => 'Skimia\\News',
    'require'     => ['skimia.pages']
];