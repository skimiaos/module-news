<?php

$app = Angular::get(OS_APPLICATION_NAME);


$app->addState('news_manager','/news-manager',function($params){
    return View::make('skimia.pages::activities.pages-manager.index',$params);
});

\Skimia\News\Data\Forms\PostsCRUDForm::register($app,'news_manager');
\Skimia\News\Data\Forms\CategoriesCRUDForm::register($app,'news_manager');
