<?php

return [


    'news' => [
        'list'=>[
            'name'    =>['label'=>'Titre de l\'actualité'],
            'slug'    =>['label'=>'Adresse seo de la page'],
            'picture'    =>['label'=>'Image de présentation'],
        ],
        'fields'=>[
            'name'             =>['label'=>'Titre de votre Actualité'],
            'category'         =>['label'=>'Catégorie de votre actualité'],
            'content_short'    =>['label'=>'Contenu court (preview)'],
            'content'          =>['label'=>'Contenu de l\'actualité'],
            'picture'             =>['label'=>'Image de présentation'],
        ],

    ],
    'categories' => [
        'list'=>[
            'name'    =>['label'=>'Titre de la Catégorie'],
            'slug'    =>['label'=>'Adresse seo de la Catégorie'],

        ],
        'fields'=>[
            'name'             =>['label'=>'Titre de votre Catégorie'],
            'isolated'             =>['label'=>'Isoler la catégorie des Actualitées classiques'],
        ],

    ],
];