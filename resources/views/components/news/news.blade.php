
<div class="row">
@foreach($actus as $actu)


            <div class="col s12 m6 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="{{$actu->picture}}">
                    </div>
                    <div class="card-content">
                        <span class="card-title grey-text text-darken-4">{{$actu->name}}</span>
                        <p><a href="{{$_links->actus_link(['slug'=>$actu->slug])}}">Dites m'en plus</a></p>

                    </div>
                </div>
            </div>




@endforeach

</div>