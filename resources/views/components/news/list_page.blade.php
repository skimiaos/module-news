
<div class="row">
    @if(!(isset($restrict_filter) && !empty($restrict_filter)))
    <div class="col s12">
        <ul style="line-height: normal">


        @foreach($categories as $cat)

            <li @if($cat->slug == Input::get('filter'))class="active" @endif>
                <a href="{{qs_url(null,['filter'=>$cat->slug])}}">{{$cat->name}}</a>
            </li>

        @endforeach
        </ul>
    </div>
    @endif
    @forelse($actus as $post)

    <div class="col s12">
        <div class="card">
            <div class="card-content grey-text">
                <span class="card-title black-text">{{$post->name}}</span>
                {{$post->content_short}}
            </div>
            <div class="card-action">
                <a href="{{$_links->actus_link(['slug'=>$post->slug])}}">Lire la suite</a>
            </div>
        </div>
    </div>
    @empty
        <div class="col s12">
            <div class="card">
                <div class="card-content grey-text">
                    <span class="card-title black-text">Aucun article</span>
                </div>
            </div>
        </div>
    @endforelse

    <div class="col s12" style="line-height: normal">
        @if(Input::has('filter'))
            {{$actus->appends(array('filter' => Input::get('filter')))->links()}}
        @else
            {{$actus->links()}}
        @endif
    </div>

</div>