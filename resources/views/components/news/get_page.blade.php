@section('meta')
    <meta property="og:title" content="{{$actu->name}}"/>
    <meta property="og:image" content="{{$actu->picture}}"/>
    <meta property="og:site_name" content="Novamap"/>
    <meta property="og:description" content="{{strip_tags($actu->content_short)}}"/>
@endsection
<article>

    <h1>{{$actu->name}}</h1>
    <img src="{{$actu->picture}}" />
    <div>
        {{$actu->content}}
    </div>

</article>

