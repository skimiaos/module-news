<?php
Event::listen('skimia.backend::seed.dashboard.default.sections', function($admin){
    return ['website'=>'Gestion du Site',];
},1000);
Event::listen('skimia.backend::seed.dashboard.default.tiles', function($admin){

    return ['website'=>[
        'news'=>[
            'static_id'=>'news',
            'size'=>'medium'
        ]
    ]];
});