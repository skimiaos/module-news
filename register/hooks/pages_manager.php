<?php
use Skimia\Backend\Managers\Bridge;

Hook::register('activities.pages_manager.sidenav-item',function(Bridge $bridge){



    $bridge->items = [
        'news'=>[
            'icon'        => 'os-icon-newspaper',
            'name'        => 'Actualités',
            'state'       => 'news_manager',
            'type'        => 'menu',
            'color'       => 'pink lighten-1',
            'items'       => [
                [
                    'icon'        => 'os-icon-flow-cascade',
                    'name'        => 'Catégories',
                    'state'       => 'news_manager.n_categories*list',
                    'description' => 'Gestion de vos catégories',
                ],
                [
                    'icon'        => 'os-icon-article-alt-1',
                    'name'        => 'Articles',
                    'state'       => 'news_manager.n_posts*list',
                    'description' => 'Gestion de vos publications',
                ]
            ],
        ]
    ];
},1500);